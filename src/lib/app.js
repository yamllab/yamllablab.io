Vue.use(VueRouter);
Vue.use(httpVueLoader);

httpVueLoader.register(Vue, './components/left-sidebar.vue');
httpVueLoader.register(Vue, './components/app-header.vue');
httpVueLoader.register(Vue, './components/feature-item.vue');
httpVueLoader.register(Vue, './components/feature-details.vue');

const routes = [
    {
        path: '/',
        component: httpVueLoader('./pages/home.vue'),
        title: 'Home',
        icon: "fa-home"
    },
    {
        path: '/services',
        component: httpVueLoader('./pages/services.vue'),
        title: "Services",
        icon: "fa-sort"
    },
    {
        path: '/features',
        component: httpVueLoader('./pages/features.vue'),
        title: "Features",
        icon: "fa-list"
    },
    {
        path: '/release-post',
        component: httpVueLoader('./pages/release-post.vue'),
        title: "Release Post",
        icon: "fa-ship"
    }
]

let router = new VueRouter({
    //mode: 'history',
    routes // short for `routes: routes`
})

router.beforeEach((to, from, next) => {
    next()
})

var app = new Vue({
    el: '#app',
    watch: {},
    mounted() {

    },
    data: {
        title: "YamlLab",
        theRoutes: routes,
        searchFilter: ''
    },
    methods: {
    },
    router
});

Vue.mixin({
    methods: {
        getByUrl(url, cb) {
            axios
            .get(url)
            .then(cb);
        },
        getFile(file, cb) {
            let url = `https://gitlab.com/api/v4/projects/gitlab-com%2Fwww-gitlab-com/repository/files/data%2F${file}.yml/raw?ref=master`;
            this.getByUrl(url, cb);
        },
        getData(file, cb) {
            this.getFile(file, response => {
                let doc = jsyaml.load(response.data);
                cb(doc);
            })
        },
        filterFreeText(arr, searchFilter) {
            return arr.filter(item => {
                return item.title.toLowerCase().includes(searchFilter.toLowerCase());
            })
        },
        filterCategory(arr) {
            //TODO: write this
            return arr
        },
        filterTier(arr, cats) {
            return arr.filter(item => {
                if (item.gitlab_core && cats.includes("core")) {
                    return item;
                }
                if (item.gitlab_starter && cats.includes("starter")) {
                    return item;
                }
                if (item.gitlab_premium && cats.includes("premium")) {
                    return item;
                }
                if (item.gitlab_ultimate && cats.includes("ultimate")) {
                    return item;
                }
                if (cats.includes("missing")) {
                    if (!item.gitlab_core && !item.gitlab_starter && !item.gitlab_premium && !item.gitlab_ultimate) {
                        return item
                    }
                }
                //console.log(item);
                //console.log(cats.toString());
                //return item
            })
        },
        categoryArray(catsObj, sort = true) {
            let cats = [];
            Object.keys(catsObj).map((objKey, index) => {
                let val = catsObj[objKey];
                val.iid = objKey;
                cats.push(val);
            })
            if (sort) {
                cats.sort((a, b) => (a.name > b.name) ? 1 : -1);
            }
            return cats
        },
        gitlabHas(feature) {
            return (feature.gitlab_core || feature.gitlab_premium || feature.gitlab_starter || feature.gitlab_ultimate)
        },
        copyText(id, me) {
            var el = document.getElementById(id);
            var range = document.createRange();
            range.selectNodeContents(el);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
            document.execCommand('copy');
            this.flipButton(me);
            sel.removeAllRanges();
            return false;
        },
        flipButton(me, pre, post) {
            if (!pre) { pre = 'btn-dark' }
            if (!post) { post = 'btn-success' }
            var btn = document.getElementById(me);
            btn.classList.add(post);
            btn.classList.remove(pre);
            setTimeout(function() {
                btn.classList.remove(post);
                btn.classList.add(pre);
            }, 2000)
        }
    }
})

//'https://gitlab.com/api/v4/projects/gitlab-com%2Fwww-gitlab-com/repository/files/data%2Ffeatures.yml/raw?ref=master'